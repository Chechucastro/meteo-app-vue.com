// ------------------------------------
// ########### IMPORTS  ###############
// ------------------------------------
import Vue from 'vue'
import Router from 'vue-router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
import axios from 'axios'

// ------------------------------------
// ########### INSTANCES  #############
// ------------------------------------
Vue.use(Router)
Vue.use(VueMaterial)

// ------------------------------------
// ########### DIRECTIVES  ############
// ------------------------------------
import '../directives/Directives.js'

// ------------------------------------
// ########### COMPONENTS  ############
// ------------------------------------
import Index from '@/components/Index'

// ------------------------------------
// ########### AXIOS  ############
// ------------------------------------
export const HTTP = axios.create({
  baseURL: 'http://localhost:8080/',
  headers: {
    Authorization: 'Bearer {token}'
  }
})

// ------------------------------------
// ########### ROUTING  ##############
// ------------------------------------
export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    }
  ]
})
