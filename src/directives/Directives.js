import Vue from 'vue'

Vue.component('favorites', {
  props: ['cityObj'],
  template: `<md-button class="md-icon-button">
              <md-icon @click.native="addFav(cityObj)" ref="hearthy">favorite</md-icon>
              </md-button>           
              `,
  data: function () {
    return {
      favoriteCities: []
    }
  },
  methods: {
    addFav: function (cityObj) {
      let thisHearth = this.$refs.hearthy.$el
      thisHearth.classList.toggle('fav')
      if (thisHearth.classList.contains('fav')) {
        console.log('Adding this city to json file')
      } else {
        console.log('Removing this city from json file')
      }
      // Get city name
      this.getCity(cityObj)
    },
    getCity (cityObj) {
      this.favoriteCities = []
      this.favoriteCities.push(cityObj)
      console.log(this.favoriteCities)
    }
  }
})

